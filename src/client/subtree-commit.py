#!/usr/bin/python
__author__ = 'Stephan Bechter <s.bechter@netconomy.net>'

import sys
import subprocess
import os

try:
    enabled = subprocess.check_output('git config hooks.subtree', shell=True).strip().lower() == "true"
    if not enabled:
        exit(0)
except:
    exit(0)

try:
    prefixValue = subprocess.check_output('git config hooks.subtree.prefixes', shell=True).strip()
except:
    print("Subtree hook not configured! Add config with ")
    print("  git config hooks.subtree.prefixes 'prefix1,prefix2'")
    print(" .. where the prefixes are the subtree prefixes.")
    exit(1)

prefixes = prefixValue.split(",")

file_result = subprocess.check_output('git diff --cached --name-only', shell=True)

if file_result == "":
    sys.exit(0)
else:
    files = file_result.split("\n")
    mainrepo = False
    subtree = []
    for fileName in files:
        if fileName.strip() == "":
            continue
        path = os.path.dirname(fileName)
        inSubtree = False
        for prefix in prefixes:
            if path.startswith(prefix):
                if not prefix in subtree:
                    subtree.append(prefix)
                inSubtree = True
                break
        if not inSubtree:
            mainrepo = True
        
        if ((mainrepo and len(subtree) > 0) or (len(subtree) > 1)):
            print
            print("commit aborted: Tried to make a overlapping commit with subtrees! Split commit!")
            print("  Involved subtree(s): %s" % subtree)
            print
            print("  (use \"git status\" to see the chages and stage only files from main or from one subtree at once")
            print
            exit(1)

