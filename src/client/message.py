#!/usr/bin/python
'''
Created on May 8, 2013

@author: Stephan Bechter <stephan@apogeum.at>
'''
import sys
import subprocess
import re

try:
    check_wordcount = subprocess.check_output('git config hooks.wordcount', shell=True).strip().lower() == "true"
except:
    check_wordcount = False
try:
    check_issue = subprocess.check_output('git config hooks.issue', shell=True).strip().lower() == "true"
except:
    check_issue = False

if check_issue:
    try:
        config_issue_key = subprocess.check_output('git config hooks.message.issuekey', shell=True).strip()
    except:
        config_issue_key = ""
if check_wordcount:
    try:
        config_word_count_str = subprocess.check_output('git config hooks.message.wordcount', shell=True).strip()
    except:
        config_word_count_str = ""

if check_wordcount and config_word_count_str == "":
    sys.stderr.write("You must configure message wordcount hook in your git config:\n")
    sys.stderr.write("\t'git config hooks.message.wordcount 3'\n")
    exit(1)

if check_issue and config_issue_key == "":
    sys.stderr.write("You must configure message issue hook in your git config:\n")
    sys.stderr.write("\t'git config hooks.message.issuekey JIRA_ISSUE_KEY[,JIRA_ISSUE_KEY2]'\n")
    exit(1)

config_word_count = 0
if check_wordcount:
    config_word_count = int(config_word_count_str)

file_name = sys.argv[1]
messageFile = open(file_name)
lines = messageFile.readlines()
messageFile.close()

_re_words = re.compile(r'\w+')

if check_issue:
    issue_keys_regex = "(NOTASK"
    config_issue_key_example = ""
    for issue_key in config_issue_key.split(","):
        issue_keys_regex += "|" + issue_key + "-\d+"
        if config_issue_key_example:
            config_issue_key_example += ", "
        config_issue_key_example += issue_key + "-123"
    issue_keys_regex += ")"
    _re_issue = re.compile(issue_keys_regex)

errorcode=0
wordcount = 0
issue_found = False

# check loop over message lines
for line in lines:
    if line.strip().startswith("#"):
        continue
    wordcount = wordcount + len(_re_words.findall(str(line)))
    if check_issue:
        issue_check = _re_issue.search(line)
        if issue_check is not None:
            if issue_check.group(1) == "NOTASK":
                wordcount = wordcount - 1
            else:
                wordcount = wordcount - 2
            issue_found = True


if check_wordcount:
    if wordcount < config_word_count:
        print("Commit message must contain at least " + str(config_word_count) + " words")
        errorcode = 1

if check_issue:
    if not issue_found:
        print("Issue (e.g.'" + config_issue_key_example + ")' not found! If no issue available use NOTASK!")
        errorcode = 2

exit(errorcode)
