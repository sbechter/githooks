#!/usr/bin/python
'''
Created on May 8, 2013

@author: Stephan Bechter <stephan@apogeum.at>
'''
import sys
import subprocess
import re
import os

try:
    enabled = subprocess.check_output('git config hooks.checkstyle', shell=True).strip().lower() == "true"
    if not enabled:
	    exit(0)
except:
    exit(0)

checkstyle_config = subprocess.check_output('git config checkstyle.config', shell=True).strip()
checkstyle_jar = subprocess.check_output('git config checkstyle.jar', shell=True).strip()
java_command = subprocess.check_output('git config java.command', shell=True).strip()
if java_command == "" :
    java_command = "java"

def isCheckstyleOk(result):
    re.search("warning:", result)
    re_warning = re.compile("warning: ")
    # regex for remove tmp dir from check result
    re_removetmppath = re.compile(r'\.git\/tmp\/')
    re_addspaceafterfile = re.compile(r'\.java:')
    output_buffer = []
    result_lines = result.split("\n")
    for line in result_lines:
        if re_warning.search(line) != None:
            cleanpath = re_removetmppath.sub("", line)
            # add space after filename (for easy seletion in shell)
            output_buffer.append(re_addspaceafterfile.sub(".java :", cleanpath))
    if not output_buffer:
        if not re.search("Audit done.", result):
            print(result_lines)
            return False
        print("All ok!")
        return True
    print("Audit failed!")
    for output in output_buffer:
        print("  " + output)
    return False

def removeDir(path):
    if not os.path.isdir(path):
        return
    files=os.listdir(path)
    for x in files:
        fullpath=os.path.join(path, x)
        if os.path.isfile(fullpath):
            #print("  - remove file " + fullpath)
            os.remove(fullpath)
        elif os.path.isdir(fullpath):
            removeDir(fullpath)
    #print("  - remove dir " + path)
    if os.path.isdir(path):
        os.removedirs(path)
    #print("  - done " + path)

if checkstyle_config == "" or checkstyle_jar == "" :
    sys.stderr.write("You must configure checkstyle in your git config:\n")
    sys.stderr.write("\t'git config --global  java.command /opt/java/bin/java' (optional if java is not in PATH)\n")
    sys.stderr.write("\t'git config --global  checkstyle.jar /dir/checkstyle-5.6-all.jar'\n")
    sys.stderr.write("\t'git config --global  checkstyle.config \"/dir/checkstyle.xml' (Local file or URL)\n") 
    sys.exit(1)

csonly = str(java_command) + " -jar " + str(checkstyle_jar)
command = str(csonly) + " -c " + str(checkstyle_config)
print(command)

file_result = subprocess.check_output('git diff --cached --name-only --diff-filter=ACM', shell=True)

if file_result == "":
    print("checkstyle: nothing to check")
    sys.exit(0)
else:
    files = file_result.split("\n")
    # cat staged files in tmp directory
    temp_dir = ".git/tmp"
    checkfiles = ""
    for item in files:
        if item.strip() == "" :
            continue
        #print("get file " +  item + " from index")
        path = temp_dir + "/" + os.path.dirname(item)
        temp_file = os.path.join(temp_dir, item)
        #print("path " +path)
        checkfiles = checkfiles + " " + temp_file
        if not os.path.exists(path):
            #print("try to create " + path)
            os.makedirs(path)
        print(subprocess.check_output("git show :" + item + " > " + temp_file, shell=True))
    
    check_command = command + " " + checkfiles
    print("Check files for checkstyle warnings")
    print("  " + subprocess.check_output(csonly + " -v", shell=True))
    for checkFile in file_result.split("\n"):
        print("  " + checkFile)
    #print(check_command)
    result = subprocess.check_output(check_command, shell=True) # + " | grep 'warning:'")
    #print("##" + result)
    # cleanup
    removeDir(temp_dir)
    if isCheckstyleOk(result) == False:
        sys.exit(1)
    
sys.exit(0)
