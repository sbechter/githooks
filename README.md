# GIT client side hooks

## Releases

### Version 1.0.2
* fixed ```pre-commit``` wrapper script to also return if 1st command fails
* BUG: fixed wrong subtree directory handling

### Version 1.0.1
* BUG: removed test code from subtree hook 

### Version 1.0.0
* added client hook to block overlapping commits with subtrees (subtree-commit.py)
* added checkstyle hook
* added message check hooks
    * Jira issue key: Force to use one of the given Jira issue keys in commit message
    * Wordcount: Min number of words in message required

## Usage

### Install python
  * **Linux**: Use your package manager.
  * **Windows**: Download the latest python 2.x installer from [https://www.python.org/downloads](https://www.python.org/downloads)
  run the installer and add the installation director to the PATH.

### Install the hooks

Execute install script from this directory for install in any git repository.

    ./installHooks.sh <directory-to-the-git-repository>

### Configuration

#### Enable/Disable Hooks

    git config hooks.issue [true/false]
    git config hooks.wordcount [true/false]
    git config hooks.subtree [true/false]
    git config hooks.checkstyle [true/false]

#### Hook configuration values

Set the required JIRA (or other issuetrackers) issue key(s). You can set a list of comma separated keys to allow this keys (e.g the project key and the support task key).

    git config hooks.message.issuekey NCA,SUP

Set the number of required words in commit message (JIRA key or NOTASK not included).

    git config hooks.message.wordcount 3

Set the git subtree prefixes to avoid overlapping hooks.

    git config hooks.subtree.prefixes prefix1,prefix2
