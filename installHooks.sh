#!/bin/sh

if [ $# -ne 1 ]; then
    echo "usage: $0 repositroy-directory"
    exit 1
fi

REPO_DIR=$1

rm $REPO_DIR/.git/hooks/*.sample
cp src/client/* $REPO_DIR/.git/hooks

echo "Enable hooks using the following commands:"
echo "Enable message check hooks"
echo "  git config hooks.issue true"
echo "  git config hooks.message.issuekey JIRAKEY  (e.g. NCA)"
echo "  git config hooks.wordcount true"
echo "  git config hooks.message.wordcount 3"
echo
echo "Enable checkstyle hook"
echo "  git config hooks.checkstyle true"
echo "  git config hooks.config <path-to-checkstyle.xml>"
echo "  git config --global hooks.jar <path-to-checkstyle-x.x-all.jar>"
echo "  git config --global java.command java"
echo
echo "Enable subtree hook"
echo "  git config hooks.subtree true"
echo "  git config subtree.prefixes \"prefix1,prefix2\""
echo
